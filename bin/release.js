#!/usr/bin/env node

try {
    require('../lib/cli')();
} catch(err) {
    console.error(err.message);
}