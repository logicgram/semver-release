((module) => {
    const cli = require('commander') || {output: null, prevBuild: null};

    const SemverRelease = require('../release');

    function startCli() {
        parseCli();

        const release = new SemverRelease({
            version: (typeof cli.version === 'string' ? cli.version : null),
            build: cli.build,
            prevBuild: cli.prevBuild,
            prevVersion: cli.prevVersion,
            preRelease: cli.preRelease,
            increment: cli.increment,
            'package': cli.package,
            incrementValue: cli.incrementValue,

            verbose: cli.verbose
        });

        if(cli.tag) {
            release.gitTag((err) => {
                if(err) throw err;
                if (cli.verbose) console.log('Successfully applied git tag');
            });

            return;
        }

        release.generateBuild();

        if (cli.output) {
            console.log(release.version);
            return;
        }

        release.writePackageVersion();
    }

    function parseCli() {
        const packageJson = require('../package.json');

        cli.description('Versioning and tagging tool for JS applications and git repos. Follows semver (semver.org)\n  Version: ' + packageJson.version)
            .arguments('[version]').action((version) => {
            cli.version = version;
        })
            .option('-V, --prev-version <prevBuild>', 'Previous build full semver version')
            .option('-B, --prev-build <prevBuild>', 'Previous build component')
            .option('-P, --pre-release <label>', 'Pre-release label (beta, alpha, ...)')
            .option('-b, --build', 'Build identifier (001, 20171026, ...)')
            .option('-i, --increment <component>', 'Increment selected semver component from --prev-build [major|minor|patch|build]', /^(major|minor|patch|build)$/i)
            .option('-I, --increment-value <int>', 'Increment the selected sevmver component by the given integer (defaults to 1)', /^-?\d+$/)
            .option('-o, --output', 'Outputs generated version to stdout without writing it')
            .option('-p, --package <package>', 'package.json file path. if set, will attempt to read previous version from it')
            .option('-t, --tag', 'Git tags the last commit *without incrementing the build*. Only uses full version+build if in pre-release')
            //.option('-T, --force-tag', "Tags the head with the new version, even if it's only a new build")
            .option('-v, --verbose', 'Verbose')
            .on('--help', function () {
                console.log();
                console.log('  Examples:');
                console.log();
                console.log('  Release mode:');
                console.log('    $ release -i major -p ./package.json');
                console.log('      >> Reads version from package.json, increments major version, resets build number and writes package.json');
                console.log('    $ release -P beta -p ./package.json');
                console.log('      >> Reads version from package.json, sets pre-release, increments build number and writes package.json');
                console.log('    $ release -B 0.1.0+20171026.025 -p ./package.json');
                console.log('      >> Reads version from prev-build, increments build number and writes package.json');
                console.log();
                console.log('  Output mode:');
                console.log('    $ release -B 0.1.0+20171026.025 -P beta -o');
                console.log('      >  0.1.0-beta+20171026.025');
                console.log('    $ release 0.1.0-alpha+20171026.025 -P beta -o');
                console.log('      > 0.1.0-beta+20171026.025');
                console.log('    $ release -B 0.1.1 -P beta -o');
                console.log('      > 0.1.1-beta+20171026.001');
                console.log('    $ release 0.1.2 -B 0.1.1+20171026.025 -o');
                console.log('      > 0.1.2+20171026.001');
                console.log('    $ release -B 0.1.1+20171026.025 -i major -o');
                console.log('      > 1.1.1+20171026.001');
                console.log();
            })
            .parse(process.argv);

        return cli;
    }

    module.exports = startCli;
})(module);