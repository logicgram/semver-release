# semver-release

## Installation

```
npm install -g
```

## Usage

### API

```js
const SemverRelease = require('semver-release');

const release = new SemverRelease({
    version: '0.4.2',
    build: '20150112.005',
    preRelease: 'beta',
    increment: 'patch',
    package: './package.json',

    verbose: false,
});

release.generateBuild();
console.log(release.version); // 0.4.3-beta+20173010.001
release.writePackageVersion();
```

### CLI

```
release [version] [options]
```

Run `release --help` for more information