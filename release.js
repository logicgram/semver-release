((module) => {
    const semverUtils = require('semver-utils'),
        moment = require('moment'),
        git = require('simple-git'),
        fs = require('fs');

    class SemverRelease {
        constructor(options) {
            this.version = options.version || null;
            this.increment = options.increment || null;
            this.preRelease = options.preRelease || null;
            this.prevBuild = options.prevBuild || null;
            this.prevVersion = options.prevVersion || null;
            this.incrementValue = (options.incrementValue && parseInt(options.incrementValue)) || 1;

            this.verbose = options.verbose || false;

            this.package = options.package || null;

            this.packageJson = null;
            this._getPackageVersion();

            return this._initVersion();
        }

        _getPackageVersion() {
            if (this.package) {
                if(this.packageJson === null) {
                    this.package = `${process.cwd()}/${this.package}`;

                    if (!fs.existsSync(this.package)) {
                        throw new Error(`package.json file not found: ${this.package}`);
                    }

                    this.packageJson = require(this.package);
                    if (!this.packageJson.version) {
                        throw new Error(`package.json was found, but not "version" field: ${this.package}`);
                    }
                }

                if (this.verbose) console.log(`Importing package.json version: ${this.packageJson.version}`);

                this.packageVersion = this.packageJson.version;
                this.prevVersion = this.prevVersion || this.packageVersion;
            }

            return this;
        }

        writePackageVersion() {
            if (this.package) {
                this._getPackageVersion();
                this.packageJson.version = this.version;

                if (this.verbose) console.log(`Writing version to package.json file: ${JSON.stringify(this.packageJson, null, ' ')}`);
                fs.writeFileSync(this.package, JSON.stringify(this.packageJson, null, '  ') + '\n');
            }

            return this;
        }

        _initVersion() {
            if (this.version) {
                if (this.verbose) console.log(`Attemting to use <version> param: ${this.version}`);
                this.semver = semverUtils.parse(this.version);

                if (this.semver === null) {
                    throw new Error(`Invalid input version: ${this.version}`);
                }
            } else if (this.prevVersion) {
                if (this.verbose) console.log(`Attemting to use prevVersion param: ${this.prevVersion}`);

                const prevSemver = semverUtils.parse(this.prevVersion);
                if (prevSemver === null) {
                    throw new Error(`Invalid input version: ${this.prevVersion}`);
                }

                this.semver = semverUtils.parse(prevSemver.version);
            } else {
                throw new Error(
                    `At least one of these arguments must be provided: [version], --prev-build <prevBuild>, --package <package>`
                );
            }

            if (this.semver === null) {
                throw new Error(`Invalid input version: ${this.version}`);
            }

            return this;
        }

        generateBuild() {
            if (this.preRelease && !this.semver.release) {
                this.semver.release = this.preRelease;
                if (this.verbose) console.log(`Setting pre-release field: ${this.semver.release}`);
            }

            this._setComponent()._setBuilderNumber();

            this.version = semverUtils.stringify(this.semver);

            return this;
        }

        _setComponent() {
            if (this.increment) {
                if (SemverRelease.versionComponents.indexOf(this.increment) === - 1) {
                    throw new Error('Invalid increment component');
                }

                if(this.increment === 'build') {
                    return this;
                }

                const newSemver = Object.assign({}, this.semver);

                // We increment the component...
                newSemver[this.increment] = parseInt(newSemver[this.increment]) + this.incrementValue;
                // ... and set to 0 the lower components, e.g. 1.2.3 -i major => 2.0.0
                SemverRelease.versionComponents.slice(
                    SemverRelease.versionComponents.indexOf(this.increment) + 1
                ).forEach((component) => {
                    newSemver[component] = 0;
                });

                if (this.verbose) console.log(`Incrementing version component ${this.increment} from ${this.semver} to ${semverUtils.stringify(newSemver)}`);
                this.semver = semverUtils.parse(semverUtils.stringify(newSemver));
            }

            return this;
        }

        _setBuilderNumber() {
            let build = null;
            if (this.semver.build) {
                build = this.semver.build;
                if (this.verbose) console.log(`Using given <version> build field: ${build}`);
            }

            if (!build) {
                let prevBuildCounter = 0;

                if(this.prevBuild) {
                    const matches = this.prevBuild.match(/\.(\d+)/);
                    if (matches && (matches[1] = parseInt(matches[1]))) {
                        prevBuildCounter = parseInt(matches[1]);
                        if (this.verbose) console.log(`Incrementing existing build number: ${prevBuildCounter}`);
                    }
                } else if (this.prevVersion) {
                    const semverPrev = semverUtils.parse(this.prevVersion);

                    if (semverPrev === null) {
                        throw new Error(`Invalid prev version: ${this.prevVersion}`);
                    }

                    // Reset build counter if new version
                    if (this.semver.version === semverPrev.version && semverPrev.build) {
                        const matches = semverPrev.build.match(/\.(\d+)/);
                        if (matches && (matches[1] = parseInt(matches[1]))) {
                            prevBuildCounter = parseInt(matches[1]);
                            if (this.verbose) console.log(`Incrementing existing build number: ${prevBuildCounter}`);
                        }
                    }
                }

                const date = moment().format('YYYYMMDD');

                let counter = prevBuildCounter + (this.increment === 'build' ? this.incrementValue : 1);
                if(counter < 1) {
                    throw new Error(`Invalid build counter: ${prevBuildCounter} + ${this.incrementValue}`);
                }
                build = `${date}.${SemverRelease.pad(prevBuildCounter + this.incrementValue, 3, 0)}`;
                if (this.verbose) console.log(`Generated build field: ${build}`);
            }

            this.semver.build = build;

            return this;
        }

        gitTag(callback) {
            const repo = git(process.cwd());

            let tag = '';
            if (!this.semver.release) {
                tag += this.semver.version;
            } else {
                tag += semverUtils.stringify(this.semver);
            }

            if (this.verbose) console.log(`Applying git tag: ${tag}`);
            repo.tag([tag], (err) => {
                callback(err, tag);
            });
        }

        // https://stackoverflow.com/questions/10073699/pad-a-number-with-leading-zeros-in-javascript
        static pad(n, width, z) {
            z = z || '0';
            n = n + '';
            return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
        }
    }

    SemverRelease.versionComponents = ['major', 'minor', 'patch', 'build'];

    module.exports = SemverRelease;
})(module);